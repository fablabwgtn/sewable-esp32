![SEW-ABLE ESP32](documentation/sewable-esp32-header.svg)
## Documentation

Welcome to the repository for Fab Lab Wgtn's Sew-able ESP32 design, a modern re-imagining of the LilyPad Arduino boards.

This board is centered around Espressif's powerful ESP32 micro-controller, contains a serial IC for easy programming via USB.

The board operates at 3.3v, allow you to connect with a wide range of existing sew-able and LilyPad boards.
Version 1 is currently only powered by USB or a 3.3v thread sewn in.
We are looking to add a LiPo battery connector and charge IC in version 2.

### Lilypad Inspired
[LilyPad](https://www.sparkfun.com/categories/135), the project that inspired the sew-able esp32, is a wearable e-textile technology developed by Leah Buechley and cooperatively designed by Leah and SparkFun. Each LilyPad was creatively designed to have large connecting pads to allow them to be sewn into clothing. Various input, output, power, and sensor boards are available. They're even washable!

### Board Features
* ESP32 micro-controller
  * Fast, Dual Processers (plus an ultra lower power co-processer)
  * Bluetooth and Wifi built in
  * 3.3V logic
* 20 sew-able GPIO
  * PWM capable
  * ADC capable
  * 2 8bit DAC
  * 10 touch sensing
* Built-in LED
* Built-in GPIO 0 Momentary Switch
* USB programming (FT231x)
* Onboard 3.3V Regulator
  * 1 Amp max current

### Sew-able ESP32 Pin Diagram
![pinout diagram](documentation/sewable-esp32-pin-diagram.svg)
